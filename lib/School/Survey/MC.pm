package School::Survey::MC;

use strict;
use warnings;
# Autor: Boris Däppen, 2018
# No guarantee given, use at own risk and will

# ABSTRACT: pick multiple choice questions randomly

use v5.6.0;

# THIS IS A STUB MODULE FOR CPAN INDEXING
# https://www.perl.com/article/how-to-upload-a-script-to-cpan/

1;

__END__

=encoding utf8

=head1 SYNOPSIS

THIS IS AN EARLY RELEASE. YOU PROBABLY SHOULD NOT USE IT, UNLESS YOU KNOW WHY.

This is a stub module for cpan indexing.
No funcionality implemented.
See L<https://www.perl.com/article/how-to-upload-a-script-to-cpan/> for the reason.

You find the implementation in the script L<mcp>.

